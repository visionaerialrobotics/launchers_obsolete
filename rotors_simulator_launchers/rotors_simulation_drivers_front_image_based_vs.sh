#!/bin/bash

NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_SWARM_ID=$3
DRONE_IP=$4
# http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
  	#Argument 2 is empty
	. ${AEROSTACK_STACK}/setup.sh
    	OPEN_ROSCORE=1
  else
    	. ${AEROSTACK_STACK}/setup.sh $2
fi
if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting droneId = 7"
    	NUMID_DRONE=7
  else
    	echo "-Setting droneId = $1"
fi
if [ -z $DRONE_SWARM_ID ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Swarm Drone ID = 1"
    	DRONE_SWARM_ID=1
  else
    	echo "-Setting Swarm Drone ID = $3"
fi
if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Setting droneIp = 192.168.1.1"
    	DRONE_IP=192.168.1.1
  else
    	echo "-Setting droneIp = $4"
fi

#xfce4-terminal  --full-screen  \
xfce4-terminal  \
	--tab --title "Driver Rotors"	--command "bash -c \"
roslaunch driverRotorsSimulatorROSModule driverRotorsSimulatorROSModuleSim.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE drone_swarm_id:=$DRONE_SWARM_ID mav_name:=hummingbird_adr my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
	--tab --title "Midlevel Controller"	--command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
	--tab --title "Drone State Estimator"	--command "bash -c \"
roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
	--tab --title "Drone Trajectory Controller"	--command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF;
						exec bash\""  \
	--tab --title "ShapeColor_ObjectDetection" --command "bash -c \"
roslaunch ShapeColor_ObjectDetection shapecolor_objectdetectionROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
	--tab --title "DroneCommunicationManager" --command "bash -c \"
roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF;
						exec bash\""  \
	--tab --title "Process Monitor"	--command "bash -c \"
roslaunch  process_monitor_process process_monitor.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
						exec bash\"" \
  --tab --title "solvepnp" --command "bash -c \"
roslaunch solvepnp solvepnp.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
            exec bash\"" &


echo "- Waiting for all process to be started..."
# wait for the modules to be running, the trick here is that rostopics blocks the execution
# until the message topic is up and running and delivers messages
rostopic echo -n 1 /drone$NUMID_DRONE/droneTrajectoryController/controlMode &> /dev/null

#---------------------------------------------------------------------------------------------
# Behavior Management
#---------------------------------------------------------------------------------------------
xfce4-terminal \
  --tab --title "Behavior Coordinator" --command "bash -c \"
    roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\""  \
  --tab --title "Behavior Specialist" --command "bash -c \"
    roslaunch behavior_specialist_process behavior_specialist_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\""  \
  --tab --title "Resource Manager" --command "bash -c \"
    roslaunch resource_manager_process resource_manager_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\""  \
  --tab --title "Belief Manager" --command "bash -c \"
    roslaunch belief_manager_process belief_manager_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\""  \
  --tab --title "Belief Updater" --command "bash -c \"
    roslaunch belief_updater_process belief_updater_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" &

#---------------------------------------------------------------------------------------------
# Behavior Catalog
#---------------------------------------------------------------------------------------------
xfce4-terminal \
  --tab --title "Behavior TakeOff" --command "bash -c \"
    roslaunch behavior_take_off behavior_take_off.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" \
  --tab --title "Behavior Land" --command "bash -c \"
    roslaunch behavior_land behavior_land.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" \
  --tab --title "Behavior Keep Hovering" --command "bash -c \"
    roslaunch behavior_keep_hovering behavior_keep_hovering.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" \
  --tab --title "Self Localization Selector" --command "bash -c \"
    roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" \
  --tab --title "Behavior Self Localize by odometry" --command "bash -c \"
    roslaunch behavior_self_localize_by_odometry behavior_self_localize_by_odometry.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
    exec bash\"" \
  --tab --title "Behavior IBVS" --command "bash -c \"
    roslaunch behavior_ibvs behavior_ibvs.launch --wait;
    exec bash\"" &

      #drone_id_namespace:=drone$NUMID_DRONE drone_id:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};

xfce4-terminal  \
	--tab --title "DroneInterface"	--command "bash -c \"
roslaunch droneInterfaceROSModule droneInterface_jp_ROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  &
