#!/bin/bash

NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_SWARM_ID=$3
DRONE_IP=$4
# http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
  	#Argument 2 is empty
	. ${AEROSTACK_STACK}/setup.sh
    	OPEN_ROSCORE=1
  else
    	. ${AEROSTACK_STACK}/setup.sh $2
fi
if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting droneId = 7"
    	NUMID_DRONE=7
  else
    	echo "-Setting droneId = $1"
fi
if [ -z $DRONE_SWARM_ID ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Swarm Drone ID = 1"
    	DRONE_SWARM_ID=1
  else
    	echo "-Setting Swarm Drone ID = $3"
fi
if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Setting droneIp = 192.168.1.1"
    	DRONE_IP=192.168.1.1
  else
    	echo "-Setting droneIp = $4"
fi

roslaunch droneInterfaceROSModule droneInterface_jp_ROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}




