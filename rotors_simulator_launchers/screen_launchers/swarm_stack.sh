#!/bin/bash

NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_SWARM_ID=$3
DRONE_IP=$4
# http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
  	#Argument 2 is empty
	. ${AEROSTACK_STACK}/setup.sh
    	OPEN_ROSCORE=1
  else
    	. ${AEROSTACK_STACK}/setup.sh $2
fi
if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting droneId = 7"
    	NUMID_DRONE=7
  else
    	echo "-Setting droneId = $1"
fi
if [ -z $DRONE_SWARM_ID ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Swarm Drone ID = 1"
    	DRONE_SWARM_ID=1
  else
    	echo "-Setting Swarm Drone ID = $3"
fi
if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Setting droneIp = 192.168.1.1"
    	DRONE_IP=192.168.1.1
  else
    	echo "-Setting droneIp = $4"
fi


screen -S rotors -X screen -t driverRotorsSimulatorROSModule_$DRONE_SWARM_ID roslaunch driverRotorsSimulatorROSModule driverRotorsSimulatorROSModuleSim.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE drone_swarm_id:=$DRONE_SWARM_ID mav_name:=hummingbird my_stack_directory:=${AEROSTACK_STACK}

screen -S rotors -X screen -t droneMidLevelAutopilotROSModule_$DRONE_SWARM_ID roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}

screen -S rotors -X screen -t droneEKFStateEstimatorROSModule_$DRONE_SWARM_ID roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}

screen -S rotors -X screen -t droneTrajectoryControllerROSModule_$DRONE_SWARM_ID roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF

screen -S rotors -X screen -t droneCommunicationManagerROSModule_$DRONE_SWARM_ID roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF

screen -S rotors -X screen -t process_monitor_process_$DRONE_SWARM_ID roslaunch  process_monitor_process process_monitor.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}

screen -S rotors -X screen -t droneManagerOfActionsROSModule_$DRONE_SWARM_ID roslaunch droneManagerOfActionsROSModule droneManagerOfActionsROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}





