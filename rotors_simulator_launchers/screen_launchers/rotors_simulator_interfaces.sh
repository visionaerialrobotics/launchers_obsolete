screen ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_interfaces.sh 7 localhost 1 # Open first screen and launch first script
split -v          # Make second split
focus             # Switch to second split
screen ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_interfaces.sh 8 localhost 2 # Open second screen and launch second script
split             # Make third split
focus             # Switch to third split
screen ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_interfaces.sh 9 localhost 3 # Open third screen and launch third script
focus             # Cycle back to first split
split           # Split first split horizontally
focus             # Switch to new split
screen #${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/swarm_interfaces.sh 10 localhost 4 # Open fourth screen and launch fourth script



