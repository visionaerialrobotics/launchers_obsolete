#Create screen environment woth name rotors and start Roscore

screen -AmdS rotors -t terminal bash
screen -S rotors -X screen -t roscore ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/start_roscore.sh

#Start Rotors Gazebo environment

screen -S rotors -X screen -t rotors_simulator_env ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/rotors_simulator_env.sh

#Start spawn of swarm agents on simulation (arg $1 == Desired number of agents in swarm)

screen -S rotors -X screen -t rotors_simulator_agents ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/rotors_simulator_agents.sh 3

#Launch the stack components for each drone in Swarm

${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_stack.sh 7 localhost 1
${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_stack.sh 8 localhost 2
${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/swarm_stack.sh 9 localhost 3

#Launch the stack Interfaces for each drone in Swarm

terminator -l ROS

screen -S swarm_drivers -c ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/rotors_simulator_interfaces.sh
