#!/bin/bash

DRONE_SWARM_MEMBERS=$1

if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting Swarm Members = 1"
    	DRONE_SWARM_MEMBERS=0
  else
    	echo "-Setting DroneSwarm Members = $1"
fi

for (( c=0; c<=$DRONE_SWARM_MEMBERS; c++ ))
do  
gnome-terminal  \
   	--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch -v rotors_gazebo mav_swarm_Leader.launch --wait drone_swarm_number:=$c mav_name:=firefly;
						exec bash\""  &
done


