#!/bin/bash

export GAZEBO_MODEL_PATH=:
export GAZEBO_PLUGIN_PATH=:



NUMID_DRONE=$1
NETWORK_ROSCORE=$2
WORLD=$3

# http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
  	#Argument 2 is empty
	. ${AEROSTACK_STACK}/setup.sh
    	OPEN_ROSCORE=1
  else
    	. ${AEROSTACK_STACK}/setup.sh $2
fi
if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting droneId = 7"
    	NUMID_DRONE=7
  else
    	echo "-Setting droneId = $1"
fi
if [ -z $WORLD ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting world = basic_arucos"
    	WORLD=basic_arucos
  else
    	echo "-Setting world = $3"
fi


#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Driver rotor simulator                                                                      ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Driver Rotors"	--command "bash -c \"
roslaunch driverRotorsSimulatorROSModule driverRotorsSimulatorROSModuleSim.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  drone_swarm_id:=$NUMID_DRONE \
  mav_name:=drone \
  my_stack_directory:=${AEROSTACK_STACK};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# MidLevel controller                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Midlevel Controller"	--command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# EKF State Estimator                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Drone State Estimator"	--command "bash -c \"
roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK};
						exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Trajectory Controller                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Drone Trajectory Controller"	--command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK} \
  drone_estimated_pose_topic_name:=estimated_pose \
  drone_estimated_speeds_topic_name:=estimated_speed;
						exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Communication Manager                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "DroneCommunicationManager" --command "bash -c \"
roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK} \
  estimated_pose_topic_name:=estimated_pose;
						exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process Monitor                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "DroneSupervisor"	--command "bash -c \"
roslaunch  process_monitor_process process_monitor.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK};
						exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Trajectory planner                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Trajectory Planner" --command "bash -c \"
roslaunch droneTrajectoryPlannerROSModule droneTrajectoryPlanner2dROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Yaw commander                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Yaw Commander" --command "bash -c \"
roslaunch droneYawCommanderROSModule droneYawCommanderROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Obstacle distance calculator                                                                ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Obstacle Distance Calculator" --command "bash -c \"
roslaunch droneObstacleDistanceCalculatorROSModule droneObstacleDistanceCalculationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Obstacle Processor                                                                          ` \
`# Identifies obstacles according to the Visual Markers                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Obstacle Processor" --command "bash -c \"
roslaunch droneObstacleProcessorVisualMarksROSModule droneObstacleProcessor2dVisualMarksROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${DRONE_STACK};
exec bash\""   \
`#---------------------------------------------------------------------------------------------` \
`# Aruco Eye                                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "ArucoEye" --command "bash -c \"
roslaunch drone_aruco_eye_ros_module droneArucoEyeROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    camera_calibration_file:=bebop2_cam_calib_480.yaml \
    image_topic_name:=/drone$NUMID_DRONE/camera_front/image_raw \
    camera_info_topic_name:=/drone$NUMID_DRONE/camera_front/camera_info;
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Visual Markers Localizer                                                                    ` \
`# Finds and recognizes Visual Markers                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Visual Marker Localizer" --command "bash -c \"
roslaunch droneVisualMarkersLocalizerROSModule droneVisualMarkersLocalizerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${DRONE_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Coordinator                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process manager                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Python based mission interpreter                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Python based mission interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    mission:=mission.py \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Task based mission planner                                                                  ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Task based mission planner" --command "bash -c \"
roslaunch task_based_mission_planner_process task_based_mission_planner_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    aruco_slam_estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  &

xfce4-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behaviors                                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
`#---------------------------------------------------------------------------------------------` \
`# Behavior TakeOff                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior TakeOff" --command "bash -c \"
roslaunch behavior_take_off behavior_take_off.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Land                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Land" --command "bash -c \"
roslaunch behavior_land behavior_land.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior GoToPoint                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior GoToPoint" --command "bash -c \"
roslaunch behavior_go_to_point behavior_go_to_point.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior FollowObjectImage                                                                  ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Follow Object Image" --command "bash -c \"
roslaunch behavior_follow_object_image behavior_follow_object_image.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Keep Hovering                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Keep Hovering" --command "bash -c \"
roslaunch behavior_keep_hovering behavior_keep_hovering.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Keep Moving                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Keep Moving" --command "bash -c \"
roslaunch behavior_keep_moving behavior_keep_moving.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Pay Attention to visual markers                                                    ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Pay Attention to Visual Markers" --command "bash -c \"
roslaunch behavior_pay_attention_to_visual_markers behavior_pay_attention_to_visual_markers.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Rotate                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Rotate" --command "bash -c \"
roslaunch behavior_rotate behavior_rotate.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Self Localize by odometry                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Self Localize by odometry" --command "bash -c \"
roslaunch behavior_self_localize_by_odometry behavior_self_localize_by_odometry.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Self Localize by Visual markers                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Self Localize by Visual Marker" --command "bash -c \"
roslaunch behavior_self_localize_by_visual_marker behavior_self_localize_by_visual_marker.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Wait                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Wait" --command "bash -c \"
roslaunch behavior_wait behavior_wait.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" &

#---------------------------------------------------------------------------------------------
# USER INTERFACE PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch rotors_gazebo env_mav.launch \
  world_name:=$WORLD;
						exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# GAZEBO                                                                                      ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch rotors_gazebo mav_swarm.launch --wait \
  world_name:=$WORLD \
  drone_swarm_number:=$NUMID_DRONE \
  mav_name:=drone;
						exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# HMI Interface                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "HMI"  --command "bash -c \"
roslaunch graphical_user_interface gui.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE  \
    drone_pose_subscription:=estimated_pose \
    drone_speeds_subscription:=estimated_speed \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# HUD                                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "HUD"  --command "bash -c \"
roslaunch first_view_process first_view.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE  \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" &

#---------------------------------------------------------------------------------------------
# SHELL INTERFACE
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
--tab --title "DroneInterface"	--command "bash -c \"
roslaunch droneInterfaceROSModule droneInterface_jp_ROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  &
